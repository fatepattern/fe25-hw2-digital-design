const headerMenu = document.querySelector(".header__nav--menu-icon");
const menuList = document.querySelector(".header__nav--expanded");
const menuListitems = document.querySelectorAll(".header__nav--expanded > li")
const icon = document.querySelector(".header__nav--menu-icon > svg")

headerMenu.addEventListener("click", function(){
    if(headerMenu.classList.contains("inactive")){
        menuList.style = "display: flex;";
        headerMenu.classList.remove("inactive");
        icon.style = "transform: rotate(180deg); transition: 0.5s;"
        headerMenu.classList.add("active");

    } else if (headerMenu.classList.contains("active")){
        menuList.style.display = "none";
        headerMenu.classList.remove("active");
        icon.style = "transform: rotate(0deg); transition: 0.5s;"
        headerMenu.classList.add("inactive");
    }
})

let w = window.innerWidth;
